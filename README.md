## qssi-user 12
12 SKQ1.220303.001 22.6.22 release-keys
- Manufacturer: xiaomi
- Platform: kona
- Codename: alioth
- Brand: POCO
- Flavor: qssi-user
- Release Version: 12
12
- Id: SKQ1.220303.001
- Incremental: 22.6.22
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Redmi/alioth/alioth:12/RKQ1.211001.001/22.6.22:user/release-keys
- OTA version: 
- Branch: qssi-user-12
12-SKQ1.220303.001-22.6.22-release-keys
- Repo: poco/alioth
